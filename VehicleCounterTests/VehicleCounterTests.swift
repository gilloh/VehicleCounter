//
//  VehicleCounterTests.swift
//  VehicleCounterTests
//
//  Created by Bruno Rodrigues on 14/07/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

import XCTest
@testable import VehicleCounter

class VehicleCounterTests: XCTestCase {
    
    let expectedName = "Bruno"
    let expectedAge = 27
    
    var marcel: Employee!
    
    override func setUp() {
        super.setUp()
        
        marcel = Employee(name: expectedName, age: expectedAge)
    }
    
    func testExampleAssertion() {
        XCTAssertEqual(1 + 1, 2, "expected one plus one to equal two")
    }
    
    func testSUT_InitializesEmployeeName() {
        XCTAssertEqual(marcel.name, expectedName)
    }
    
    func testSUT_InitializesEmployeeRank() {
        XCTAssertEqual(marcel.age, expectedAge)
    }
}
