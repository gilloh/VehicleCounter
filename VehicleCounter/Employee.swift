//
//  Employee.swift
//  VehicleCounter
//
//  Created by Bruno Rodrigues on 14/07/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

struct Employee: Equatable {
    
    let name: String
    let age: Int
}
