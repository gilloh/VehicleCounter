//
//  VehicleCounterViewControllerSpec.swift
//  VehicleCounter
//
//  Created by Bruno Rodrigues on 14/07/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

import Quick
import Nimble

@testable import VehicleCounter

class VehicleCounterViewControllerSpec: QuickSpec {
    
    override func spec() {
        
        var sut: VehicleCounterViewController!
        
        beforeEach {
            sut = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController() as! VehicleCounterViewController
            _ = sut.view
        }
        
        context("when the view loads") {
            it("should show a counter label that starts with zero") {
                expect(sut.counterLabel.text).to(equal(String(0)))
            }
            
            it("should show a button named increase") {
                expect(sut.increaseButton.titleLabel?.text).to(equal("increase"))
            }
            
            it("should show a button named decrease") {
                expect(sut.decreaseButton.titleLabel?.text).to(equal("decrease"))
            }
        }
        
        context("the counter label") {
            it("should show the current vehicle counter") {
                sut.counter = 3
                expect(sut.counterLabel.text).to(equal(String(3)))
            }
            
            it("should hould increase by one by pressing the increase button") {
                sut.counter = 2
                sut.increaseAction(sut.increaseButton)
                expect(sut.counterLabel.text).to(equal(String(3)))
            }
            
            it("should hould decrease by one by pressing the decrease button") {
                sut.counter = 5
                sut.decreaseAction(sut.decreaseButton)
                expect(sut.counterLabel.text).to(equal(String(4)))
            }
        }
        
        context("the decrease button") {
            it("should be disabled if the counter is zero") {
                expect(sut.decreaseButton.isEnabled).to(beFalse())
            }
        }
        
        context("the increase button") {
            it("should be disabled if the counter hits maximum capacity") {
                sut.counter = sut.maxCapacity
                expect(sut.increaseButton.isEnabled).to(beFalse())
            }
        }
    }
}
