//
//  VehicleCounterViewController.swift
//  VehicleCounter
//
//  Created by Bruno Rodrigues on 14/07/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

import UIKit

class VehicleCounterViewController: UIViewController {

    @IBOutlet weak var counterLabel: UILabel!
    
    @IBOutlet weak var decreaseButton: UIButton!
    @IBOutlet weak var increaseButton: UIButton!
    
    let maxCapacity = 5
    
    var counter: Int = 0 {
        didSet {
            counterLabel.text = String(counter)
            decreaseButton.isEnabled = !(counter == 0)
            increaseButton.isEnabled = !(counter == maxCapacity)
        }
    }

    @IBAction func increaseAction(_ sender: Any) { counter += 1 }
    @IBAction func decreaseAction(_ sender: Any) { counter -= 1 }
}

