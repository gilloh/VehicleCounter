//
//  EmployeeSpec.swift
//  VehicleCounterTests
//
//  Created by Bruno Rodrigues on 14/07/2018.
//  Copyright © 2018 Bruno. All rights reserved.
//

import Quick
import Nimble

@testable import VehicleCounter

class EmployeeSpec: QuickSpec {
    
    override func spec() {
        var sut: Employee!
        
        let expectedName = "Bruno"
        let expectedAge = 27
        
        beforeEach {
            sut = Employee(name: expectedName, age: expectedAge)
        }
        
        context("initializing an Employee object") {
            it("should have the same name as the expected name") {
                expect(sut.name).to(equal(expectedName))
            }
            
            it("should have the same age as the expected age") {
                expect(sut.age).to(equal(expectedAge))
            }
        }
    }
}
